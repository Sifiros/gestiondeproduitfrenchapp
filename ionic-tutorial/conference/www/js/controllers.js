angular.module('controllers', ['services', 'base64'])

.controller('MenuCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  //var authProvider = 'basic';
  //var authSettings = { 'remember': true };

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('LoginCtrl', function($base64, $scope) {

  $scope.loginData = {};

  //var authProvider = 'basic';
  //var authSettings = { 'remember': true };

  $scope.doLogin = function() {
    var tmp = $scope.loginData.username + ':' + $scope.loginData.password;
    var encoded = $base64.encode(tmp);
    var test = 'Basic ' + encoded;
    console.log('Doing login' , test);
}

})

.controller('ProductCtrl', function($scope, $stateParams, Product) {
  $scope.product = Product.getProd();
  })

.controller('ProductsCtrl', function($scope, $ionicModal, Product) {
  //$scope.products = Product.query();

  // Create the login modal

  $scope.newProduct = {};

  $ionicModal.fromTemplateUrl('templates/addProduct.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  //Open the adding modal

  $scope.addProduct = function() {
    $scope.modal.show();
  };

  // Close the adding modal

  $scope.closeAddProduct = function() {
    $scope.modal.hide();
  };

  $scope.doAddProduct = function() {
    console.log('adding product', $scope.newProduct);
  }
});
