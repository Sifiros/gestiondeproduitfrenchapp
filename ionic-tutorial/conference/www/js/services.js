angular.module('services', [])

.factory('globalUrl', function()
{
  return {
    gl_ProdUrl : 'http://192.168.0.47:1337/products',
    gl_UserUrl : 'http://192.168.0.47:1337/user'
  };
})

.factory('Product', function ($http, globalUrl) {
  var product = {};
  product.getAllProd = function() {

    $http( {
      method: 'GET',
      url: 'http://192.168.0.47:1337/products'
    })
    .then(function successCb(response) {
      console.log(globalUrl.gl_ProdUrl);
      console.log(response.data);
    }, function errorCb(error) {
      console.log(error);
    });
  };

  product.getProd = function() {
    // defer
    $http( {
      method: 'GET',
      url: 'http://192.168.0.47:1337/product/577e11401424f78d053df345'
    })
    .then(function successCb(response) {
      // return
      console.log(response.data);
    }, function errorCb(error) {
      console.log(error);
    });
    //
  };

  return (product);
  //  return $resource('http://192.168.0.47:1337/product');
});
